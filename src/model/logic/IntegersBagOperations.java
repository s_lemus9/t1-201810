package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin(IntegersBag bag) {
		int min = Integer.MAX_VALUE;
		int val;
		if(bag != null) {
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()) {
				val = iter.next();
				if(val < min) {
					min = val;
				}
			}
		}
		return min;
	}
	
	public double computeStandardDeviation(IntegersBag bag) {
		double mean = computeMean(bag);
		int size = bag.getSize();
		double sum = 0.0;
		int val;
		
		if(bag != null) {
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()) {
				val = iter.next();
				sum += Math.pow(val - mean, 2);
			}
		}
		double sd = size > 0 ? Math.sqrt(sum/size) : -1;
		return sd;
	}
	
	public int computeRange(IntegersBag bag) {
		return getMax(bag) - getMin(bag);
	}
	
	public double computeSumOfSquares(IntegersBag bag) {
		double sum = 0.0;
		Iterator<Integer> iter = bag.getIterator();
		
		while(iter.hasNext()) {
			sum += Math.pow(iter.next(), 2);
		}
		return sum;
	}
}
